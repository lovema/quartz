﻿using System.Data;
using Dapper;

namespace Raise.Workbench.Common {
    /// <summary>
    /// 针对系统方法进行扩展
    /// </summary>
    public static class PersistenceLog {
        /// <summary>
        /// 进行日志记录，
        /// </summary>
        /// <param name="dbConnection">数据库连接</param>
        /// <param name="parameters">parameters为SysLog类型，需要包含 message，loglevel,stacktrace callsite 字段</param>
        public static void ExecuteLogRecord(this IDbConnection dbConnection, object parameters) {
            dbConnection.Execute(
                "Insert into syslog(id,message,createTime,loglevel,stacktrace,callsite)values(s_syslog.nextval,:message,sysdate,:loglevel,:stacktrace,:callsite)",
                parameters);
        }
    }
}
