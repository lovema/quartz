﻿using System;
using System.Collections.Specialized;
using System.Configuration;
using NLog;

namespace Raise.Workbench.Service {
    /// <summary>
    /// Quartz服务的配置
    /// </summary>
    public class Configuration {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger(typeof(Configuration));

        private const string PrefixServerConfiguration = "quartz.server";
        private const string KeyServiceName = PrefixServerConfiguration + ".serviceName";
        private const string KeyServiceDisplayName = PrefixServerConfiguration + ".serviceDisplayName";
        private const string KeyServiceDescription = PrefixServerConfiguration + ".serviceDescription";
        private const string KeyServerImplementationType = PrefixServerConfiguration + ".type";

        private const string DefaultServiceName = "Workbench";
        private const string DefaultServiceDisplayName = "Workbench Service";
        private const string DefaultServiceDescription = "PMS作业调度服务";
        private static readonly string DefaultServerImplementationType = typeof(QuartzService).AssemblyQualifiedName;

        private static readonly NameValueCollection ConfigurationPrivate;

        /// <summary>
        /// 初始化<see cref ="Configuration"/>类。
        /// </summary>
		static Configuration() {
            try {
                ConfigurationPrivate = (NameValueCollection)ConfigurationManager.GetSection("quartz");
            } catch(Exception e) {
                Logger.Error("获取配置节失败: " + e.Message);
            }
        }

        /// <summary>
        /// 获取服务的名称.
        /// </summary>
        /// <value>服务的名称.</value>
		public static string ServiceName => GetConfigurationOrDefault(KeyServiceName, DefaultServiceName);

        /// <summary>
        /// 获取服务的显示名称.
        /// </summary>
        /// <value>服务的显示名称.</value>
        public static string ServiceDisplayName => GetConfigurationOrDefault(KeyServiceDisplayName, DefaultServiceDisplayName);

        /// <summary>
        /// 获取服务描述.
        /// </summary>
        /// <value>服务描述。</value>
        public static string ServiceDescription => GetConfigurationOrDefault(KeyServiceDescription, DefaultServiceDescription);

        /// <summary>
        /// 获取服务器实现的类型名称。
        /// </summary>
        /// <value>服务器实现的类型。</value>
        public static string ServerImplementationType => GetConfigurationOrDefault(KeyServerImplementationType, DefaultServerImplementationType);

        /// <summary>
        /// 使用给定键返回配置值。 如果配置对于不存在，返回默认值。
        /// </summary>
        /// <param name="configurationKey">用于读取配置的关键.</param>
        /// <param name="defaultValue">如果找不到配置，则返回默认值</param>
        /// <returns>配置值.</returns>
        private static string GetConfigurationOrDefault(string configurationKey, string defaultValue) {
            string retValue = null;
            if(ConfigurationPrivate != null) {
                retValue = ConfigurationPrivate[configurationKey];
            }

            if(retValue == null || retValue.Trim().Length == 0) {
                retValue = defaultValue;
            }
            return retValue;
        }
    }
}
