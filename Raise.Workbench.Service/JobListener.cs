﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Quartz;

namespace Raise.Workbench.Service {
    /// <summary>
    /// 监听器
    /// </summary>
    public class JobListener : IJobListener {
        public virtual string Name {
            get {
                return "监听器，暂时不适用";
            }
        }

        public Task JobToBeExecuted(IJobExecutionContext context, CancellationToken cancellationToken = default(CancellationToken)) {
            return Task.Factory.StartNew(() => Console.WriteLine("JOB执行前"), cancellationToken);
        }

        public Task JobExecutionVetoed(IJobExecutionContext context, CancellationToken cancellationToken = default(CancellationToken)) {
            return Task.Factory.StartNew(() => Console.WriteLine("JobExecutionVetoed"), cancellationToken);
        }

        public Task JobWasExecuted(IJobExecutionContext context, JobExecutionException jobException, CancellationToken cancellationToken = default(CancellationToken)) {
            return Task.Factory.StartNew(() => Console.WriteLine("JOB执行"), cancellationToken);
        }
    }
}